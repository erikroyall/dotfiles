ln -s $PWD/.vimrc ~/.vimrc
ln -s $PWD/.eslintrc.json ~/.eslintrc.json
ln -s $PWD/.gitconfig ~/.gitconfig
ln -s $PWD/.npmrc ~/.npmrc
ln -s $PWD/.tern-config ~/.tern-config
ln -s $PWD/.Xresources ~/.Xresources
ln -s $PWD/.ycm_extra_conf.py ~/.ycm_extra_conf.py
ln -s $PWD/.zshrc ~/.zshrc

mkdir -p ~/.vim/swaps ~/.vim/undo ~/.vim/backups

