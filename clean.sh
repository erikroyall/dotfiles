rm ~/.vimrc
rm ~/.ycm_extra_conf.py
rm ~/.tern-config
rm ~/.eslintrc.json
rm ~/.Xresources
rm ~/.npmrc
rm ~/.gitconfig
rm ~/.zshrc
