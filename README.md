# My dotfiles

## Installation

```sh
git clone https://gitlab.com/erikroyall/dotfiles.git
cd dotfiles
bash init.sh
```

## Removing symlinks

```sh
bash clean.sh
```

**Warning**: Running `clean.sh` removes all your configuration files

